<?php

namespace Quentin\BaBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Quentin\BaBundle\Entity\User;
use Quentin\BaBundle\Entity\Item;

class DefaultController extends Controller
{
  public function mainAction()
  {
    $items = $this->getDoctrine()->getRepository('QuentinBaBundle:Item')->findAll();
    $actions = array();
    foreach($items as $item)
      $actions = $item->getName();

    //$recentActions = array("recent action 1", "recent action 2");
    return $this->render('QuentinBaBundle:Main:main.html.twig', array('recentActions' => $actions));
  }

  public function newItemAction(Request $request)
  {
    $item = new Item();
    $item->setConsumable(true);
    $form = $this->createFormBuilder($item)
      ->add('name', 'text')
      ->add('consumable', 'checkbox')
      ->add('expiryDate', 'date')
      ->add('save', 'submit')
      ->add('amount', 'number')
      ->add('price', 'number')
      ->getForm();
    $em = $this->getDoctrine()->getManager();
    $form->handleRequest($request);

    if ($form->isValid()) {
      $em->persist($item);
      $em->flush();

      return $this->redirect($this->generateUrl('quentin_ba_main'));
    }

    return $this->render('QuentinBaBundle:Bricks:form.html.twig', array('form' => $form->createView(),));
  }

  public function removeItemAction($item)
  {
    return $this->redirect($this->generateUrl('quentin_ba_main'));
  }

  public function authenticationAction()
  {
    $session = $request->getSession();
    $number = 3;
    $session->set('connectionNumber',$number);
    $id = 3.14;
    $session->set('id',$id);
    $userId = $session->get('id');
  }

  public function settingsAction()
  {
    //retrieve settings
    return $this->render('QuentinBaBundle:Main:settings.html.twig');
  }

  public function showUsersAction()
  {
    $users = $this->getDoctrine()->getRepository('QuentinBaBundle:User')->findAll();
    $names = array();
    foreach($users as $user)
      $names[] = $user->getFirstName();

    return $this->render('QuentinBaBundle:Main:users.html.twig', array('users' => $names));
  }

  public function newUserAction(Request $request)
  {
    $user = new User();
    $user->setFirstName('Dummy');
    $form = $this->createFormBuilder($user)
      ->add('firstName', 'text')
      ->add('lastName', 'text')
      ->add('save', 'submit')
      ->getForm();
    $em = $this->getDoctrine()->getManager();
    $form->handleRequest($request);

    if ($form->isValid()) {
      $em->persist($user);
      $em->flush();

      return $this->redirect($this->generateUrl('quentin_ba_main'));
    }

    return $this->render('QuentinBaBundle:Default:newuser.html.twig', array('form' => $form->createView(),));
  }

  public function removeUserAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('QuentinBaBundle:User')->find($id);
    if ($user) {
      $em->remove($user);
      $em->flush();
    }

    return new Response();
  }

  public function navigationBarAction()
  {
    return $this->render('QuentinBaBundle:Default:navigationBar.html.twig');
  }

  public function adminAction()
  {
    return new Response('hi');
  }
}
