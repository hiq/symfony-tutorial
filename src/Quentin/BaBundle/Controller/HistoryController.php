<?php

namespace Quentin\BaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HistoryController extends Controller
{
  public function recentAction($days = "1")
  {
    return $this->render('QuentinBaBundle:History:recent.html.twig', array('days' => $days));
  }

  public function historyAction($day = 0)
  {
    //TODO: retrieve actions of $day, and put them in $actions
    $actions = array("first act", "second act");

    return $this->render('QuentinBaBundle:History:history.html.twig', array('actions' => $actions));
  }
}
