<?php

namespace Quentin\BaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 *
 */
class UserRepository extends EntityRepository
{
  public function findAllByFirstName($firstName)
  {
    return $em = $this->getEntityManager();
    $query = $em->createQueryBuilder('p')
      ->where('p.firstName = :fn')
      ->setParameter(fn, $firstName)
      ->getQuery();

    return $query->getResult();
  }
}
