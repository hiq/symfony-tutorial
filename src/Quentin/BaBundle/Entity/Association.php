<?php

namespace Quentin\BaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Association
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Quentin\BaBundle\Entity\AssociationRepository")
 */
class Association
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=255)
   */
  private $name;

  /**
   * @ORM\OneToMany(targetEntity="User", mappedBy="association")
   **/
  private $users;

  /**
   * @ORM\OneToMany(targetEntity="Item", mappedBy="association")
   **/
  private $items;

  /**
   * @ORM\OneToMany(targetEntity="ItemGroup", mappedBy="association")
   **/
  private $itemGroups;

  /**
   * @ORM\OneToOne(targetEntity="Wallet")
   **/
  private $wallet;

  public function __construct()
  {
    $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    $this->items = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   *
   * @param string $name
   * @return Association
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }
}
