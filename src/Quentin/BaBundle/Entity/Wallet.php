<?php

namespace Quentin\BaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wallet
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Quentin\BaBundle\Entity\WalletRepository")
 */
class Wallet
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="money", type="decimal")
   */
  private $money;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set money
   *
   * @param string $money
   * @return Wallet
   */
  public function setMoney($money)
  {
    $this->money = $money;

    return $this;
  }

  /**
   * Get money
   *
   * @return string
   */
  public function getMoney()
  {
    return $this->money;
  }
}
