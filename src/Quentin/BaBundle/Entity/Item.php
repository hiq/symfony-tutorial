<?php

namespace Quentin\BaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Quentin\BaBundle\Entity\ItemRepository")
 */
class Item
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=255)
   */
  private $name;

  /**
   * @var boolean
   *
   * @ORM\Column(name="consumable", type="boolean")
   */
  private $consumable;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="expiryDate", type="datetime")
   */
  private $expiryDate;

  /**
   * @var string
   *
   * @ORM\Column(name="amount", type="decimal")
   */
  private $amount;

  /**
   * @var string
   *
   * @ORM\Column(name="price", type="decimal")
   */
  private $price;

  /**
   * @ORM\ManyToOne(targetEntity="Association", inversedBy="items")
   **/
  private $association;

  /**
   * @ORM\ManyToMany(targetEntity="ItemGroup", mappedBy="items")
   **/
  private $itemGroups;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   *
   * @param string $name
   * @return Item
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Get name
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set consumable
   *
   * @param boolean $consumable
   * @return Item
   */
  public function setConsumable($consumable)
  {
    $this->consumable = $consumable;

    return $this;
  }

  /**
   * Get consumable
   *
   * @return boolean
   */
  public function getConsumable()
  {
    return $this->consumable;
  }

  /**
   * Set expiryDate
   *
   * @param \DateTime $expiryDate
   * @return Item
   */
  public function setExpiryDate($expiryDate)
  {
    $this->expiryDate = $expiryDate;

    return $this;
  }

  /**
   * Get expiryDate
   *
   * @return \DateTime
   */
  public function getExpiryDate()
  {
    return $this->expiryDate;
  }

  /**
   * Set amount
   *
   * @param string $amount
   * @return Item
   */
  public function setAmount($amount)
  {
    $this->amount = $amount;

    return $this;
  }

  /**
   * Get amount
   *
   * @return string
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * Set price
   *
   * @param string $price
   * @return Item
   */
  public function setPrice($price)
  {
    $this->price = $price;

    return $this;
  }

  /**
   * Get price
   *
   * @return string
   */
  public function getPrice()
  {
    return $this->price;
  }
}
